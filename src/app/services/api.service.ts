import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  serverUrl: any = 'http://192.168.1.84:8080/api/products/';

  constructor(
    public http: HttpClient
  ) { }

  get() {
    return this.http.get(this.serverUrl);
  }

  post(data) {
    return this.http.post(this.serverUrl, data);
  }

  put(data) {
    return this.http.put(this.serverUrl, data);
  }

  delete(id) {
    return this.http.delete(this.serverUrl + id);
  }
}
