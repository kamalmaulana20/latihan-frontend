import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { ProductDetailComponent } from '../product-detail/product-detail.component';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  books: any = [];

  constructor(public dialog: MatDialog, public api: ApiService) { }

  ngOnInit(): void {
    console.log(this.api.get())
    this.getBooks();
  }

  loading: boolean;
  getBooks() {
    this.loading = true;
    this.api.get().subscribe(result => {
      this.books = result;
      this.loading = false;
    }, error => {
      this.loading = false;
      alert('Ada Masalah Saat pengambilan Data, Coba Lagi!');
    });
  }

  productDetail(data, idx) {
    const dialog = this.dialog.open(ProductDetailComponent, {
      width: '400px',
      data: data
    });

    dialog.afterClosed().subscribe(result => {
      if (result) {
        //jika idx=-1 (penambahan data baru)
        if (idx == -1) this.books.push(result);
        //jika tidak maka perbarui
        else this.books[idx] = data;
      }
    });
  }

  loadingDelete: any = {};
  deleteProduct(id, idx) {
    var conf = confirm('Delete item?');
    if (conf) {
      this.loadingDelete[idx] = true;
      this.api.delete(id).subscribe(result => {
        this.books.splice(idx, 1);
        this.loadingDelete = false;
      }, error => {
        this.loadingDelete[idx] = false;
        alert('Tidak dapat Menghapus Data.')
      });
    }
  }
}
