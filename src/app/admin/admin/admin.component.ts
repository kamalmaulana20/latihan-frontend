import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(public logout: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  menu = [
    {
      name: 'Dashboard',
      icon: 'dashboard',
      url: '/admin/dashboard'
    },
    {
      group: 'Menu Group',
      children: [
        {
          name: 'Image Gallery',
          icon: 'images',
          url: '/admin/gallry'
        },
        {
          name: 'Product',
          icon: 'add_circle',
          url: '/admin/product'
        }
      ]
    }
  ];

  _logout() {
    this.logout.logout();
    this.router.navigate(["/"]);
  }

}
