import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(username: string, password: string): boolean {
    if ("kamal" == username && "123" == password) {
      let userInfo = {
        fullName: "Kamal Maulana Adha",
        username: "kamal"
      };

      localStorage.setItem("userInfo", JSON.stringify(userInfo));
      localStorage.setItem("token", "kopiscript111");
      return true;
    }
    return false;
  }
  logout(): void {
    localStorage.removeItem("userInfo");
    localStorage.removeItem("token");
  }
}
