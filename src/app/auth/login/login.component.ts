import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router, private _snackBar: MatSnackBar) { }

  email: string;
  password: string;

  ngOnInit(): void {
  }

  login() {
    console.log("Username : " + this.email);
    console.log("Password : " + this.password);


    if (this.auth.login(this.email, this.password)) {
      console.log("Login Sukses");
      this.router.navigate(["/admin"]);
      this._snackBar.open("Suskes", "close", { duration: 5000 })
    } else {
      this._snackBar.open("GAGAL", "close", { duration: 2000 })
    }

  }
}
